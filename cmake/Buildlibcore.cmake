FetchContent_Declare(
    libcore
    GIT_REPOSITORY https://github.com/educelab/libcore.git
    GIT_TAG dff1fd8
    EXCLUDE_FROM_ALL
)
FetchContent_MakeAvailable(libcore)